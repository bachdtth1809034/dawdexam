package com.example.dawdexam;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import com.example.exercise11.database.AppDatabase;
import com.example.exercise11.database.BookmarkEntity;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    AppDatabase database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        database = AppDatabase.getAppDatabase(this);
        insertBookmark();
        updateBookmark(2);
        getAllBookmark();
        findBookmark(1);
        deleteBookmark(3);
        deleteAllBookmark();
    }

    private void deleteAllBookmark() {
        database.bookmarkDAO().deleteAll();
    }

    private void deleteBookmark(int id) {
        BookmarkEntity entity = database.bookmarkDAO().getBookmark(id);
        if(entity != null) {
            database.bookmarkDAO().deleteBookmark(entity);
        }
    }

    private void findBookmark(int id) {
        BookmarkEntity entity = database.bookmarkDAO().getBookmark(id);
        Log.d("TAG", "Find bookmark with id "+entity.getId() + ", title: " + entity.getTitle());
    }

    private void getAllBookmark() {
        List<BookmarkEntity> entities = database.bookmarkDAO().getAllBookmark();
        for(BookmarkEntity entity : entities) {
            Log.d("TAG", "id "+entity.getId() + ", title: " + entity.getTitle());
        }
    }

    private void updateBookmark(int id) {
        BookmarkEntity entity = database.bookmarkDAO().getBookmark(id);
        entity.setTitle("This is title update");
        database.bookmarkDAO().editBookmark(entity);
    }

    private void insertBookmark() {
        BookmarkEntity entity = new BookmarkEntity();
        entity.setTitle("This is title");
        entity.setContent("This is content");
        database.bookmarkDAO().insertBookmark(entity);
    }


}
